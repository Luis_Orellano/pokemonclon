extends Node2D

export (PackedScene) var player
export (PackedScene) var nivel1
export (PackedScene) var nivel2
export (PackedScene) var nivel3

var nivel_actual = 1
var cas_n

func _ready():
	spawn_lvl_player(nivel1,0)

func check_door(casillero):
	$fade.play("fadeout")
	print(casillero)
	cas_n = casillero

func spawn_lvl_player(lvl,n_spawn):
	var newLvl = lvl.instance()
	add_child(newLvl)
	var newPlayer = player.instance()
	add_child(newPlayer)
	newPlayer.global_position = get_tree().get_nodes_in_group("spawn")[n_spawn].global_position


func _on_fade_animation_finished(anim_name):
	if(anim_name == "fadeout"):
		$fade.play("fadein")
		get_tree().get_nodes_in_group("player")[0].queue_free()
		get_tree().get_nodes_in_group("nivel")[0].free()
		match(nivel_actual):
			1:
				nivel_actual = 2
				spawn_lvl_player(nivel2,0)
			2:
				if(cas_n == Vector2(3,-2)):
					nivel_actual = 1
					spawn_lvl_player(nivel1,1)
				if(cas_n == Vector2(-1,4) || cas_n == Vector2(-2,4)):
					nivel_actual = 3
					spawn_lvl_player(nivel3,0)
