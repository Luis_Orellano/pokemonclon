extends KinematicBody2D

var vel_actual = Vector2()
var vel_desp = 16
var main

func _ready():
	main = get_tree().get_nodes_in_group("main")[0]

func _physics_process(delta):
	if(!$AnimationPlayer.is_playing()):
		if(Input.is_action_just_pressed("tecla_x")):
			interactuar_tile()
		elif(Input.is_action_pressed("tecla_w")):
			$detector.rotation_degrees = 0
			vel_actual.y = -vel_desp * delta / $AnimationPlayer.get_animation("walk_up").length
			$AnimationPlayer.play("walk_up")
		elif(Input.is_action_pressed("tecla_s")):
			vel_actual.y = vel_desp * delta / $AnimationPlayer.get_animation("walk_down").length
			$AnimationPlayer.play("walk_down")
			$detector.rotation_degrees = 180
		elif(Input.is_action_pressed("tecla_a")):
			vel_actual.x = -vel_desp * delta / $AnimationPlayer.get_animation("walk_side").length
			$AnimationPlayer.play("walk_side")
			$Sprite.flip_h = false
			$detector.rotation_degrees = 270
		elif(Input.is_action_pressed("tecla_d")):
			vel_actual.x = vel_desp * delta / $AnimationPlayer.get_animation("walk_side").length
			$AnimationPlayer.play("walk_side")
			$Sprite.flip_h = true
			$detector.rotation_degrees = 90
		
		var n_tile = get_tree().get_nodes_in_group("tile")[0].get_cellv(get_tree().get_nodes_in_group("tile")[0].world_to_map($detector/pos.global_position))
		if(n_tile != -1):
			if(n_tile != 17):
				if(n_tile != 3):
					if(n_tile != 4):
						vel_actual = Vector2(0,0)
	
	if($AnimationPlayer.is_playing()):
		move_and_collide(vel_actual)


func _on_AnimationPlayer_animation_finished(anim_name):
	vel_actual = Vector2(0,0)
	global_position = Vector2(
	round((round(global_position.x - get_tree().get_nodes_in_group("spawn")[0].global_position.x)
	/vel_desp)*vel_desp)+get_tree().get_nodes_in_group("spawn")[0].global_position.x,
	round((round(global_position.y - get_tree().get_nodes_in_group("spawn")[0].global_position.y)
	/vel_desp)*vel_desp)+get_tree().get_nodes_in_group("spawn")[0].global_position.y)
	
	var n_tile = get_tree().get_nodes_in_group("tile")[0].get_cellv(get_tree().get_nodes_in_group("tile")[0].world_to_map(global_position))
	if(n_tile == 17 || n_tile == 4 || n_tile == 3): #estoy en escalera o puerta
		main.check_door(get_tree().get_nodes_in_group("tile")[0].world_to_map(global_position))
	

func interactuar_tile():
	var n_tile = get_tree().get_nodes_in_group("tile")[0].get_cellv(get_tree().get_nodes_in_group("tile")[0].world_to_map($detector/pos.global_position))
	print(n_tile)
	match(n_tile):
		1: #PC
			pass
		14: #consola videojuegos
			pass
